// Создаём пустой обьект

let student = {
  name: "",
  lastName: "",
  table: {},
};

/* Сщздаём переменные для name and lastname, 
которые будут спрашивать у пользователя name and lastname*/

student.name = prompt("Enter your name");
student.lastName = prompt("Enter your lastname");

// Студент вводит название предмета и оценку по нём

while (true) {
  let lessonName = prompt("Enter your lessonname");

  if (!lessonName) {
    break;
  }

  let assessment = prompt("Enter your assessment");

  student.table[lessonName] = assessment;
}

// Сщздаём 3 переменные

let badAssessmentsCount = 0;
let sumAssessments = 0;
let countAssessments = 0;

// Cчитаем количество плохих оценок

for (let lessonName in student.table) {
  let assessment = parseInt(student.table[lessonName]);

  if (assessment < 4) {
    badAssessmentsCount++;
  }
  sumAssessments += assessment;
  countAssessments++;
}

// Виводим результат в консоль

console.log("Name", student.name);
console.log("Lastname", student.lastName);
console.log("Table assessment", student.table);
console.log("Number bad assessment", badAssessmentsCount);

/*Делаем проверку и выводим текст оповещения в консоль, 
что студент перешол на следующий курс*/

if (badAssessmentsCount === 0) {
  console.log("Student transferred the next course");
}

/*Сщздаём переменную и делаем проверку, какой средний балл студента*/

let averageAssessment = sumAssessments / countAssessments;
console.log("Average assessment student", averageAssessment);

// Делаем проверку получит ли студент стипендию

if (averageAssessment > 7) {
  console.log("Student will receive scholarship");
}
